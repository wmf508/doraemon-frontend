import type { NextPage } from 'next'
import Head from 'next/head'

const Home: NextPage = () => {
  return (
    <div>
      <Head>
        <title>Doraemon</title>
        <meta name="description" content="doraemon is super cool" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <div>test main content</div>
      </main>

      <footer>
        <div>test footer</div>
      </footer>
    </div>
  )
}

export default Home
